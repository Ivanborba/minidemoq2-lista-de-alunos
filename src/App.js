import { Component } from "react";
import "./App.css";
import CardsList from "./components/cards-list";
import Header from "./components/header";

class App extends Component {
  state = {
    students: [
      {
        name: "Ivan Borba",
        grade: 5,
      },
      {
        name: "Matheus Gasparotto",
        grade: 10,
      },
      {
        name: "Larissa Pinheiro",
        grade: 7,
      },
      {
        name: "João Silva",
        grade: 9,
      },
      {
        name: "José Santos",
        grade: 8,
      },
      {
        name: "Lucas Oliveira",
        grade: 5,
      },
      {
        name: "Bruno Souza",
        grade: 4,
      },
      {
        name: "Marina Schmidt",
        grade: 5,
      },
      {
        name: "Natália Koch",
        grade: 6,
      },
      {
        name: "Alexia Martins",
        grade: 7,
      },
    ],
    showAllStudents: true,
    showApprovedStudents: false,
    showFailedStudents: false,
  };

  handleAllStudents = () => {
    this.setState({
      showAllStudents: true,
      showApprovedStudents: false,
      showFailedStudents: false,
    });
  };

  handleApprovedStudents = () => {
    this.setState({
      showAllStudents: false,
      showApprovedStudents: true,
      showFailedStudents: false,
    });
  };

  handleFailedStudents = () => {
    this.setState({
      showAllStudents: false,
      showApprovedStudents: false,
      showFailedStudents: true,
    });
  };

  render() {
    const showApprovedStudents = this.state.students.filter(
      (student) => student.grade >= 6
    );
    const showFailedStudents = this.state.students.filter(
      (student) => student.grade < 6
    );
    return (
      <div className="App">
        <Header
          handleAllStudents={this.handleAllStudents}
          handleApprovedStudents={this.handleApprovedStudents}
          handleFailedStudents={this.handleFailedStudents}
        />
        {this.state.showAllStudents && (
          <CardsList students={this.state.students} />
        )}
        {this.state.showApprovedStudents && (
          <CardsList students={showApprovedStudents} />
        )}
        {this.state.showFailedStudents && (
          <CardsList students={showFailedStudents} />
        )}
      </div>
    );
  }
}

export default App;
