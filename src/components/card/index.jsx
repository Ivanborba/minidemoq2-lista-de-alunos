import { Component } from "react";

class Card extends Component {
  render() {
    return (
      <div>
        {this.props.children}
        <p
          style={
            this.props.student.grade < 6 ? { color: "red" } : { color: "green" }
          }
        >
          {this.props.student.grade}
        </p>
      </div>
    );
  }
}

export default Card;
