import { Component } from "react";
import Card from "../card";

class CardsList extends Component {
  state = {
    filteredStudents: [],
    input: "",
  };

  handleInput = (e) => {
    this.setState({
      input: e.target.value,
      filteredStudents: this.props.students.filter((actual) =>
        actual.name.toLowerCase().includes(e.target.value.toLowerCase())
      ),
    });
  };

  render() {
    return (
      <div>
        <input
          placeholder="Buscar alunos"
          value={this.state.input}
          onChange={this.handleInput}
        />
        {this.state.filteredStudents.length !== 0
          ? this.state.filteredStudents.map((student, index) => {
              return (
                <Card student={student} key={index}>
                  <p>{student.name}</p>
                </Card>
              );
            })
          : this.props.students.map((student, index) => {
              return (
                <Card student={student} key={index}>
                  <p>{student.name}</p>
                </Card>
              );
            })}
      </div>
    );
  }
}

export default CardsList;
