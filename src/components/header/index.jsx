import { Component } from "react";

class Header extends Component {
  render() {
    return (
      <div>
        <button onClick={this.props.handleAllStudents}>
          Mostrar todos os alunos
        </button>
        <button onClick={this.props.handleApprovedStudents}>
          Mostrar os alunos aprovados
        </button>
        <button onClick={this.props.handleFailedStudents}>
          Mostrar os alunos reprovados
        </button>
      </div>
    );
  }
}

export default Header;
